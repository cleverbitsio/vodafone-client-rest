#!/bin/bash

# https://unix.stackexchange.com/questions/191934/how-to-check-the-string-is-null-or-not-in-shell-script
if [ ! -z "$(cat ~/pid.txt)" ] ; then
	pkill -P $(cat ~/pid.txt) # this is needed for MACOS (the parent process)
	kill -9 $(cat ~/pid.txt) # pkill isn't working on centos as expected - and in fact - the PID I get is for the actual java process - so we can use standard kill -9
fi
#command ping -i 5 google.com > ~/log.txt & # works
#java -cp /home/terry/vodafone-load-generator/target/quick-start-1.0-SNAPSHOT.jar com.cleverbits.basics.CassandraLoadGenerator -w $(cat ~/wps.txt) -r $(cat ~/rps.txt) > ~/log.txt &
#java -cp /home/terry/vodafone-load-generator/target/quick-start-1.0-SNAPSHOT.jar com.cleverbits.basics.CassandraLoadGenerator -w $(cat ~/wps.txt) > ~/log.txt &
java -cp ~/vodafone-load-generator/target/quick-start-1.0-SNAPSHOT.jar com.cleverbits.basics.CassandraLoadGenerator -w $(cat ~/wps.txt) -n $(cat ~/nodes.txt) -r > ~/log.txt &
echo $! > ~/pid.txt # since this script starts the background process above - its own PID will be stored here
