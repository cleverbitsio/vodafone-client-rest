#!/bin/bash

# https://unix.stackexchange.com/questions/191934/how-to-check-the-string-is-null-or-not-in-shell-script
if [ ! -z "$(cat ~/pid.txt)" ] ; then
	pkill -P $(cat ~/pid.txt) # this is needed for MACOS (the parent process)
	kill -9 $(cat ~/pid.txt) # pkill isn't working on centos as expected - and in fact - the PID I get is for the actual java process - so we can use standard kill -9
fi
