package CassandraClientREST.model;

/**
 * Created by terry on 06/09/2017.
 */
public class CassandraLoadGenConfig {

    private final long id;
    private final Integer writespersecond;
    private final Integer readspersecond;

    public CassandraLoadGenConfig(long id, Integer writespersecond, Integer readspersecond) {
        this.id = id;
        this.writespersecond = writespersecond;
        this.readspersecond = readspersecond;
    }

    public long getId() {
        return id;
    }

    public Integer getWritespersecond() {
        return writespersecond;
    }
    public Integer getReadspersecond() {
        return readspersecond;
    }
}
