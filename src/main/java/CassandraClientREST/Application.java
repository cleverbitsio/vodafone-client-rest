package CassandraClientREST;

import CassandraClientREST.utils.BashUtilities;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@SpringBootApplication
@SpringBootConfiguration
public class Application {

    @Bean
    ApplicationLifeCycleBean myBean() {
        return new ApplicationLifeCycleBean();
    }

    private static class ApplicationLifeCycleBean {

        @PostConstruct
        public void init() {
            System.out.println("init");
        }

        public void doSomething() {
            System.out.println("in doSomething()");
        }

        @PreDestroy
        public void destroy() {
            System.out.println("destroy");
            String userHome = System.getenv("HOME");
            BashUtilities.runBashFile(userHome + "/shutdownMyBackgroundTasks.sh");
        }
    }

    public static void main(String[] args) {

        //SpringApplication.run(Application.class, args);
        ApplicationContext context = SpringApplication.run(Application.class, args);
        ApplicationLifeCycleBean myBean = context.getBean(ApplicationLifeCycleBean.class);
        myBean.doSomething();

    }
}
