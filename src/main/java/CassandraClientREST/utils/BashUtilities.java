package CassandraClientREST.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by terry on 06/09/2017.
 */
public class BashUtilities {
    public static void runBashFile(String bashFile) {
        try {
            String target = new String(bashFile); // String target = new String("mkdir stackOver");
            Runtime rt = Runtime.getRuntime();
            Process proc = rt.exec(target);
            proc.waitFor();

            StringBuffer output = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String line = "";
            while ((line = reader.readLine())!= null) {
                output.append(line + "\n");
            }
            System.out.println(output);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
}
