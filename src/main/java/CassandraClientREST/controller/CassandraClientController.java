package CassandraClientREST.controller;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicLong;

import CassandraClientREST.model.CassandraLoadGenConfig;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static CassandraClientREST.utils.BashUtilities.runBashFile;

@CrossOrigin
@RestController
public class CassandraClientController {

    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/runBashFile") //http://localhost:8080/runBashFile?writepersecond=1&nodes=node.c.vodafone-cassandra-poc.internal,node2.c.vodafone-cassandra-poc.internal
    public CassandraLoadGenConfig startApp  (   @RequestParam(value="writespersecond", defaultValue="18") Integer writespersecond,
                                                @RequestParam(value="readspersecond", defaultValue="0")Integer readspersecond,
                                                @RequestParam(value="nodes", defaultValue="node.c.vodafone-cassandra-poc.internal,node2.c.vodafone-cassandra-poc.internal")String nodes
                                            ) {

        String userHome = System.getenv("HOME");
        try {
            FileWriter fw = new FileWriter(userHome + "/wps.txt");
            PrintWriter pw = new PrintWriter(fw);
            pw.println(writespersecond);
            pw.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            FileWriter fw2 = new FileWriter(userHome + "/rps.txt");
            PrintWriter pw2 = new PrintWriter(fw2);
            pw2.println(readspersecond);
            pw2.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            FileWriter fw3 = new FileWriter(userHome + "/nodes.txt");
            PrintWriter pw3 = new PrintWriter(fw3);
            pw3.println(nodes);
            pw3.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //call bash
        //check pid file in case another instance is running - if so kill
        //start app as background with new writespersecond and readspersecond
        //set pid in file
        runBashFile(userHome + "/test.sh");

        return new CassandraLoadGenConfig(counter.incrementAndGet(),
                writespersecond, readspersecond);
    }
}

